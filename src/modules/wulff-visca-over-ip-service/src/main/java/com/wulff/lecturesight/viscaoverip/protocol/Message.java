package com.wulff.lecturesight.viscaoverip.protocol;

import com.wulff.lecturesight.viscaoverip.protocol.VISCA.MessageType;
import com.wulff.lecturesight.viscaoverip.protocol.VISCA.PayloadType;

public class Message implements Cloneable {

  PayloadType payloadType;
  MessageType messageType; // used when reacting to responses
  byte[] data;   // message template

  public Message(VISCA.PayloadType payloadType, MessageType messageType) {
    this.payloadType = payloadType;
    this.messageType = messageType;
  }

  public PayloadType getPayloadType() {
    return payloadType;
  }
  
  public MessageType getMessageType(){
    return messageType;
  }
  
  public Message(VISCA.PayloadType payloadType, MessageType messageType, int[] data) {
    this.payloadType = payloadType;
    this.messageType = messageType;
    this.data = ByteUtils.i2b(data);
  }
  
  public Message setbyteValue(int index, byte value) {
      Message out = clone();
      out.data[ VISCA.CMD_LENGTH + index ] = value;
      return out;
  }
  
  public Message setshortValue(int index, short value) {
      byte [] data = ByteUtils.s2b(value);
      Message out = clone();
      for (int i = 0; i< data.length; i++){
          out.data [ VISCA.CMD_LENGTH + index + i ] = data[i];
      }
      return out;
  }

  public byte[] getBytes() {
    return data;
  }

  public void setAddress(int adr) {
    data[0] += adr;
  }

  @Override
  public Message clone() {
    Message clone = new Message(payloadType,messageType);
    clone.data = new byte[this.data.length];
    System.arraycopy(data, 0, clone.data, 0, data.length);
    return clone;
  }
  
  public String toString(){
	  String s = this.payloadType +"";
	  return s;
  }
}
