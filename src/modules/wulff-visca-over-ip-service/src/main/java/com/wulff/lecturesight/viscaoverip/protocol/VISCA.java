package com.wulff.lecturesight.viscaoverip.protocol;

public class VISCA {

  public static enum PayloadType {
    VISCA_COMMAND(0x01, 0x00),
    VISCA_INQUIRY(0x01, 0x10), 
    VISCA_REPLY(0x01,0x11),
    VISCA_SET_DEVICE(0x01,0x20),
    CONTROL_COMMAND(0x02, 0x00), 
    CONTROL_REPLY(0x02, 0x01),
    ILLEGAL_MESSAGE(0x99, 0x99);
    
    private byte[] code;
    PayloadType(int one, int two) {
        code = new byte[2];
        code[0] = (byte)one;
        code[1] = (byte)two;
    }
    public byte[] getCode() { 
        return code;
    }
    
    public String toString(){
    	switch(code[0]){
	    	case 0x01:
	    		switch (code[1]){
	    			case 0x00: return "VISCA COMMAND";
	    			case 0x10: return "VISCA INQUIRY";
	    			case 0x11: return "VISCA REPLY";
	    			case 0x20: return "VISCA SET DEVICE";
	    		}
	    	case 0x02:
	    		switch (code[1]){
	    			case 0x00: return "CONTROL COMMAND";
	    			case 0x01: return "CONTROL REPLY";
	    		}	    	
    	}
    	return "Illegal MessageType";
    }
  }


  
  /**
   * Enum of error types in VISCA.
   *
   */
  public static enum ErrorType{
    SYNTAX_ERROR,CMD_BUFFER_FULL,CMD_CANCELLED,NO_SOCKET,CMD_NOT_EXECUTABLE,UNKNOWN
  }

  public static final int TERMINATOR = 0xff;
  public static final int ADR_CAMERA_N = 0x81; //Fixed address for VISCA over IP
  public static final int DATA = 0x00;

  public static final int DEFAULT_SPEED = 0x01;
  
  public static final int CMD_LENGTH = 4;

  // definition of byte sequences of VISCA messages
  public static final int[] CODE_IfClear = {ADR_CAMERA_N, 0x01, 0x00, 0x01, TERMINATOR};
  public static final int[] CODE_CommandCancel = {ADR_CAMERA_N, 0x20, TERMINATOR};
  public static final int[] CODE_CamVersion_Inq = {ADR_CAMERA_N, 0x09, 0x00, 0x02, TERMINATOR};
  public static final int[] CODE_PanTiltPos_Inq = {ADR_CAMERA_N, 0x09, 0x06, 0x12, TERMINATOR};
  public static final int[] CODE_ZoomPos_Inq = {ADR_CAMERA_N, 0x09, 0x04, 0x47, TERMINATOR};
  public static final int[] CODE_FocusPos_Inq = {ADR_CAMERA_N, 0x09, 0x04, 0x48, TERMINATOR};
  public static final int[] CODE_MoveHome_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x04, TERMINATOR};
  public static final int[] CODE_MoveAbsolute_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x02, DATA, DATA,  DATA, DATA,  DATA, DATA,  DATA, DATA,  DATA, DATA, TERMINATOR};
  public static final int[] CODE_MoveRelative_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x03, DATA, DATA,  DATA, DATA,  DATA, DATA,  DATA, DATA,  DATA, DATA, TERMINATOR};
  public static final int[] CODE_MoveUp_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x03, 0x01, TERMINATOR};
  public static final int[] CODE_MoveDown_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x03, 0x02, TERMINATOR};
  public static final int[] CODE_MoveLeft_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x01, 0x03, TERMINATOR};
  public static final int[] CODE_MoveRight_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x02, 0x03, TERMINATOR};
  public static final int[] CODE_MoveUpLeft_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x01, 0x01, TERMINATOR};
  public static final int[] CODE_MoveUpRight_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x02, 0x01, TERMINATOR};
  public static final int[] CODE_MoveDownLeft_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x01, 0x02, TERMINATOR};
  public static final int[] CODE_MoveDownRight_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x02, 0x02, TERMINATOR};
  public static final int[] CODE_StopMove_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x01, DATA, DATA, 0x03, 0x03, TERMINATOR};
  public static final int[] CODE_LimitSet_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x07, 0x00, DATA, DATA, DATA,  DATA, DATA, DATA, DATA,  DATA, DATA, TERMINATOR};
  public static final int[] CODE_LimitClear_Cmd = {ADR_CAMERA_N, 0x01, 0x06, 0x07, 0x01, DATA, 0x07, 0x0f,  0x0f, 0x0f, 0x07, 0x0f,  0x0f, 0x0f, TERMINATOR};
  public static final int[] CODE_Zoom_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x47, DATA, DATA, DATA, DATA, TERMINATOR};
  public static final int[] CODE_FocusStop_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x08, 0x00, TERMINATOR};
  public static final int[] CODE_FocusFar_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x08, 0x02, TERMINATOR};
  public static final int[] CODE_FocusNear_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x08, 0x03, TERMINATOR};
  public static final int[] CODE_FocusAuto_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x38, 0x02, TERMINATOR};
  public static final int[] CODE_FocusManual_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x38, 0x03, TERMINATOR};
  public static final int[] CODE_FocusDirect_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x48, DATA, DATA, DATA, DATA, TERMINATOR};
  

  // Non-standard VISCA commands (Vaddio cameras, maybe others)
  public static final int[] CODE_MemoryReset_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x3F, 0x00, DATA, TERMINATOR};
  public static final int[] CODE_MemorySet_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x3F, 0x01, DATA, TERMINATOR};
  public static final int[] CODE_MemoryRecall_Cmd = {ADR_CAMERA_N, 0x01, 0x04, 0x3F, 0x02, DATA, TERMINATOR};
  
  public static final int[] CODE_Reset = {0x01,TERMINATOR};

  // Control Commands
  public static final Message CTRL_RESET = new Message(PayloadType.CONTROL_COMMAND, MessageType.CTRL_RESET, CODE_Reset);
  
  // Network messages __________________________________________________________
  public static final Message NET_IF_CLEAR = new Message(PayloadType.VISCA_COMMAND, MessageType.NET_IF_CLEAR, CODE_IfClear);
  public static final Message NET_COMMAND_CANCEL = new Message(PayloadType.VISCA_COMMAND, MessageType.NET_COMMAND_CANCEL, CODE_CommandCancel);

  // Inquiry messages ___________________________________________________________
  public static final Message INQ_CAM_VERSION = new Message(PayloadType.VISCA_INQUIRY, MessageType.INQ_CAM_VERSION, CODE_CamVersion_Inq);
  public static final Message INQ_PAN_TILT_POS = new Message(PayloadType.VISCA_INQUIRY, MessageType.INQ_PAN_TILT_POS, CODE_PanTiltPos_Inq);
  public static final Message INQ_ZOOM_POS = new Message(PayloadType.VISCA_INQUIRY, MessageType.INQ_ZOOM_POS, CODE_ZoomPos_Inq);
  public static final Message INQ_FOCUS_POS = new Message(PayloadType.VISCA_INQUIRY, MessageType.INQ_FOCUS_POS, CODE_FocusPos_Inq);
  
  // Command messages __________________________________________________________
  public static final Message CMD_MOVE_HOME = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_HOME, CODE_MoveHome_Cmd);
  public static final Message CMD_MOVE_UP = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_UP, CODE_MoveUp_Cmd);
  public static final Message CMD_MOVE_DOWN = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_DOWN, CODE_MoveDown_Cmd);
  public static final Message CMD_MOVE_LEFT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_LEFT, CODE_MoveLeft_Cmd);
  public static final Message CMD_MOVE_RIGHT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_RIGHT, CODE_MoveRight_Cmd);
  public static final Message CMD_MOVE_UP_LEFT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_UP_LEFT, CODE_MoveUpLeft_Cmd);
  public static final Message CMD_MOVE_UP_RIGHT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_UP_RIGHT, CODE_MoveUpRight_Cmd);
  public static final Message CMD_MOVE_DOWN_LEFT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_DOWN_LEFT, CODE_MoveDownLeft_Cmd);
  public static final Message CMD_MOVE_DOWN_RIGHT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_DOWN_RIGHT, CODE_MoveDownRight_Cmd);
  public static final Message CMD_MOVE_ABSOLUTE = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_ABSOLUTE, CODE_MoveAbsolute_Cmd);
  public static final Message CMD_MOVE_RELATIVE = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_RELATIVE, CODE_MoveRelative_Cmd);
  public static final Message CMD_STOP_MOVE = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_STOP_MOVE, CODE_StopMove_Cmd);
  public static final Message CMD_LIMIT_SET = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_LIMIT_SET, CODE_LimitSet_Cmd);
  public static final Message CMD_LIMIT_CLEAR = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_LIMIT_CLEAR, CODE_LimitClear_Cmd);
  public static final Message CMD_ZOOM = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_ZOOM, CODE_Zoom_Cmd);
  public static final Message CMD_FOCUS_STOP = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_STOP, CODE_FocusStop_Cmd);
  public static final Message CMD_FOCUS_FAR = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_FAR, CODE_FocusFar_Cmd);
  public static final Message CMD_FOCUS_NEAR = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_NEAR, CODE_FocusNear_Cmd);
  public static final Message CMD_FOCUS_AUTO = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_AUTO, CODE_FocusAuto_Cmd);
  public static final Message CMD_FOCUS_MANUAL = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_MANUAL, CODE_FocusManual_Cmd);
  public static final Message CMD_FOCUS_DIRECT = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_FOCUS_DIRECT, CODE_FocusDirect_Cmd);
  
  // Non-standard VISCA commands (Vaddio cameras, maybe others)
  public static final Message CMD_MOVE_PRESET = new Message(PayloadType.VISCA_COMMAND, MessageType.CMD_MOVE_PRESET, CODE_MemoryRecall_Cmd);

  public static enum MessageType {
    CTRL_RESET, NET_IF_CLEAR, NET_COMMAND_CANCEL, INQ_CAM_VERSION, INQ_PAN_TILT_POS, INQ_ZOOM_POS, INQ_FOCUS_POS,
    CMD_MOVE_HOME, CMD_MOVE_UP, CMD_MOVE_DOWN, CMD_MOVE_LEFT, CMD_MOVE_RIGHT, CMD_MOVE_UP_LEFT, CMD_MOVE_UP_RIGHT,
    CMD_MOVE_DOWN_LEFT, CMD_MOVE_DOWN_RIGHT, CMD_MOVE_ABSOLUTE, CMD_MOVE_RELATIVE, CMD_STOP_MOVE, CMD_LIMIT_SET,
    CMD_LIMIT_CLEAR, CMD_ZOOM, CMD_FOCUS_STOP, CMD_FOCUS_FAR, CMD_FOCUS_NEAR, CMD_FOCUS_AUTO, CMD_FOCUS_MANUAL, 
    CMD_FOCUS_DIRECT, CMD_MOVE_PRESET
  }


  
}
