package com.wulff.lecturesight.viscaoverip.service;

public class Constants {

  // settings read from the global configuration
  final static String PROPKEY_DEVICE_IP = "device.ip";
  final static String PROPKEY_DEVICE_PORT = "device.port";
  final static String PROPKEY_UPDATER_INTERVAL = "updater.interval";
  final static String PROPKEY_UPDATER_POLL_FOCUS = "updater.poll.focus";

  // Settings read from the profile files in /profile
  final static String PROFKEY_VENDOR_ID = "camera.vendor.id";
  final static String PROFKEY_VENDOR_NAME = "camera.vendor.name";
  final static String PROFKEY_MODEL_ID = "camera.model.id";
  final static String PROFKEY_MODEL_NAME = "camera.model.name";
  final static String PROFKEY_PAN_MIN = "camera.pan.min";
  final static String PROFKEY_PAN_MAX = "camera.pan.max";
  final static String PROFKEY_PAN_MAXSPEED = "camera.pan.maxspeed";
  final static String PROFKEY_TILT_MIN = "camera.tilt.min";
  final static String PROFKEY_TILT_MAX = "camera.tilt.max";
  final static String PROFKEY_TILT_MAXSPEED = "camera.tilt.maxspeed";
  final static String PROFKEY_ZOOM_MIN = "camera.zoom.min";
  final static String PROFKEY_ZOOM_MAX = "camera.zoom.max";
  final static String PROFKEY_ZOOM_MAXSPEED = "camera.zoom.maxspeed";
  final static String PROFKEY_HOME_PAN = "camera.home.pan";
  final static String PROFKEY_HOME_TILT = "camera.home.tilt";
  final static String PROFKEY_SOCKETS_COUNT = "camera.sockets.count";

}
