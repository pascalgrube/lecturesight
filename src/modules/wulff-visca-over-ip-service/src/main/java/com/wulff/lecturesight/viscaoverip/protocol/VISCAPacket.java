package com.wulff.lecturesight.viscaoverip.protocol;

public class VISCAPacket {
  Message message;
  byte[] payload;
  long inFlight = -1;
  int sequenceNumber = -1;
  boolean acknowledged = false;

  public Message getMessage() {
    return message;
  }

  public void setMessage(Message message) {
    this.message = message;
  }

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(byte[] payload) {
    this.payload = payload;
  }

  public long getInFlight() {
    return inFlight;
  }

  public void setInFlight(long inFlight) {
    this.inFlight = inFlight;
  }

  public int getSequenceNumber() {
    return sequenceNumber;
  }

  public void setSequenceNumber(int sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
  }

  public boolean isAcknowledged() {
    return acknowledged;
  }

  public void setAcknowledged(boolean acknowledged) {
    this.acknowledged = acknowledged;
  }

}
