package com.wulff.lecturesight.viscaoverip.service;

import com.wulff.lecturesight.visca.api.VISCAService;
import com.wulff.lecturesight.viscaoverip.protocol.ByteUtils;
import com.wulff.lecturesight.viscaoverip.protocol.VISCAPacket;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.pmw.tinylog.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import cv.lecturesight.ptz.api.PTZCamera;
import cv.lecturesight.util.conf.Configuration;

@Component(name = "com.bwulff.lecturesight.visca", immediate = true)
@Service
public class VISCAServiceImpl implements VISCAService {

  ComponentContext cc;

  // TODO: Move to camera properties
  final int VISCAPort = 52381; // set by spec

  final int lostPacketTimeout = 100;

  @Reference
  Configuration config; // service configuration

  // Camera profiles
  Properties defaultProfile;
  Map<String, Properties> cameraProfiles = new HashMap<String, Properties>();

  DatagramSocket UDPSocket;
  MessageReciever UDPlistener;
  Thread UDPlistenerThread;

  HashMap<InetAddress, VISCACameraImpl> cameras = new HashMap<InetAddress, VISCACameraImpl>();

  int updateInterval = 20; // min number of millisec. between state updates on a camera
  int senderInterval = 20;
  int config_timeout = 3000; // ms within which camera must reply to initial inquiry

  boolean updatePollFocus = false;

  private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
  ScheduledFuture updaterHandle;
  ScheduledFuture senderHandle;

  /**
   * OSGI service activation method.
   *
   * @param cc
   */
  protected void activate(ComponentContext cc) throws Exception {
    this.cc = cc;

    Logger.debug("Activated");

    // load device profiles
    loadProfiles(cc);

    // initialize UDP socket listener
    init_UDPlistener();

    // TODO get camera from config
    createCamera(InetAddress.getByName(config.get(Constants.PROPKEY_DEVICE_IP)), 52381, "0x0503");

    // start UDP listener thread
    UDPlistener = new MessageReciever();
    UDPlistenerThread = new Thread(UDPlistener);
    UDPlistenerThread.start();

    // start update thread
    updaterHandle = executor.scheduleAtFixedRate(new CameraStateUpdater(), updateInterval, updateInterval,
            TimeUnit.MILLISECONDS);

    // start sender thread
    senderHandle = executor.scheduleAtFixedRate(new CameraCommandSender(), senderInterval, senderInterval,
            TimeUnit.MILLISECONDS);

    Logger.info("Completed VISCA over IP camera initialization");
  }

  /**
   * OSGI service de-activation method.
   *
   * @param cc
   */
  protected void deactivate(ComponentContext cc) {
    try {
      UDPlistener.running = false;
      Thread.sleep(500);
      if (UDPlistenerThread.isAlive())
        UDPlistenerThread.interrupt();
      executor.shutdown();
      executor.awaitTermination(1, TimeUnit.SECONDS);
      Thread.sleep(1500);
    } catch (Exception e) {
      Logger.debug("Unable to terminate scheduled processes cleanly");
    }

    // Unregister camera
    ServiceReference serviceReference = cc.getBundleContext().getServiceReference(PTZCamera.class.getName());
    cc.getBundleContext().ungetService(serviceReference);

    deinit_UDPlistener();

    Logger.debug("Deactivated");
  }

  void init_UDPlistener() throws SocketException {
    UDPSocket = new DatagramSocket(VISCAPort);
  }

  void deinit_UDPlistener() {
    UDPSocket.close();
  }

  /**
   * Load the camera model profiles from the bundle resources into memory.
   *
   * @param context
   */
  void loadProfiles(ComponentContext context) {
    // load device profiles
    Enumeration entryURLs = context.getBundleContext().getBundle().findEntries("profiles", "*.properties", false);
    while (entryURLs.hasMoreElements()) {
      URL url = (URL) entryURLs.nextElement();
      try {
        Properties props = new Properties();
        props.load(url.openStream());
        if (props.containsKey(Constants.PROFKEY_MODEL_ID)) {
          String idStr = props.getProperty(Constants.PROFKEY_MODEL_ID);
          if (idStr.equals("DEFAULT")) {
            defaultProfile = props;
            Logger.info("Registered default camera profile");
          } else {
            cameraProfiles.put(idStr, props);
            Logger.info("Registered camera profile for " + props.getProperty(Constants.PROFKEY_VENDOR_NAME) + " "
                    + props.getProperty(Constants.PROFKEY_MODEL_NAME));
          }
        } else {
          Logger.warn("Camera profile " + url.toString() + " does not contain model ID!");
        }
      } catch (IOException e) {
        Logger.warn("Failed to load device profile from " + url.toString() + " : " + e.getMessage());
      }
    }
  }

  /**
   * Creates a camera object that represents a registered VISCA devices and puts it into the array representing the
   * seven possible addresses.
   *
   * @param adr
   *          address of device
   * @param msg
   *          VersionInq message from device
   */
  private void createCamera(InetAddress adr, int port, String model) {

    // extract camera information from VISCA message
    // String model = "0x" + ByteUtils.byteToHex(msg[4]) + ByteUtils.byteToHex(ByteUtils.low(msg[5]));
    // String rom_ver = "0x" + ByteUtils.byteToHex(msg[6]) + ByteUtils.byteToHex(msg[7]);
    String rom_ver = "Who cares";
    // int sockets = msg[16];

    // try to find camera profile, if none found use default profile
    Properties profile;
    if (cameraProfiles.containsKey(model)) {
      profile = cameraProfiles.get(model);
    } else {
      profile = defaultProfile;
    }

    String modelStr = profile.getProperty("camera.vendor.name") + " " + profile.getProperty("camera.model.name");
    Logger.info("Registered " + modelStr + " (model ID " + model + ", ROM version " + rom_ver + ") at address #" + adr);

    // register camera
    VISCACameraImpl camera = new VISCACameraImpl(adr, port, profile);
    camera.parent = this;
    cameras.put(adr, camera);

    // register camera as OSGI service
    String camName = camera.model_name + " [" + adr + "]";
    Dictionary props = new Hashtable();
    props.put("adr", adr); // TODO look up / change places where former 'id' key is used
    props.put("name", camName);
    props.put("type", "VISCA");
    //props.put("port", config.get(Constants.PROPKEY_PORT_DEVCICE));

    cc.getBundleContext().registerService(PTZCamera.class.getName(), camera, props);
  }

  // TODO replace with System.arrayCopy (?)
  void copyPart(byte[] src, byte[] dest, int offset, int length) {
    int glength = offset + length;
    int dp = 0;
    for (int sp = offset; sp < glength; sp++) {
      dest[dp++] = src[sp];
    }
  }

  /**
   * Called when a UDP message was received. Parses the message and acts accordingly.
   *
   * @param arg0
   */
  public void packetReceived(DatagramPacket packet) {

    byte[] buffer = new byte[packet.getLength()];
    copyPart(packet.getData(), buffer, packet.getOffset(), packet.getLength());

    // debug output
    Logger.trace(" <<" + packet.getAddress() + "length:" + packet.getLength() + " :: "
            + ByteUtils.byteArrayToHex(buffer, packet.getLength()) + ":: ");

    cameras.get(packet.getAddress()).handleResponse(buffer);

  }

  /**
   * Threads that frequently sends inquiries to all registered cameras.
   *
   */
  class CameraStateUpdater implements Runnable {
    @Override
    public void run() {
      try {
        for (InetAddress adr : cameras.keySet()) {
          VISCACameraImpl camera = cameras.get(adr);
          camera.updatePosition();
        }
      } catch (Exception e) {
        throw new IllegalStateException("Exception running camera state updater. ", e);
      }
    }
  }

  /**
   * Thread that sends commands to the registered cameras.
   *
   */
  class CameraCommandSender implements Runnable {

    @Override
    public void run() {
      for (InetAddress adr : cameras.keySet()) {
        VISCACameraImpl camera = cameras.get(adr);
        camera.fillPending();
        if(System.currentTimeMillis()  - camera.lastPacketSent > 10 )
        	if(camera.pendingCommand != null && camera.pendingInquiry != null){
	        		if(camera.pendingCommand.getSequenceNumber() < camera.pendingInquiry.getSequenceNumber()){        	
	        		sendCommand(camera);
	        	} else {
	        		sendInquiry(camera);
	        	}
			} else {
				if(camera.pendingCommand != null){
					sendCommand(camera);
				}
				if(camera.pendingInquiry != null){
					sendInquiry(camera);
				}
		
			}
      }
    }
    private boolean sendInquiry(VISCACameraImpl camera){
    	boolean sent = false;
        if (null != camera.pendingInquiry) {
            VISCAPacket message = camera.pendingInquiry;
            if (lostPacketTimeout < System.currentTimeMillis() - message.getInFlight()) {
              // if VISCAPacket has not been sent, set to waiting
              byte[] payload = message.getPayload();
              if (-1 == message.getInFlight()) {
                camera.issuedMsg.put(message.getSequenceNumber(), message);
              } else {
                Logger.error("Packet got lost: " + ByteUtils.byteArrayToHex(payload, payload.length));
              }            
              try {
                DatagramPacket sendPacket = new DatagramPacket(payload, payload.length, camera.address, camera.getPort());
                UDPSocket.send(sendPacket);
                message.setInFlight(System.currentTimeMillis());
                camera.lastPacketSent = System.currentTimeMillis();
                camera.lastSentWasInquiry =true;
                Logger.trace("Packet sent: " + ByteUtils.byteArrayToHex(payload, payload.length));
                sent = true;
              } catch (IOException e) {
                Logger.error("Error while sending data: " + ByteUtils.byteArrayToHex(payload, payload.length), e);
              }
            }
        }    	
        return sent;
    }
    
    private boolean sendCommand(VISCACameraImpl camera){
    	boolean sent = false;
        if (null != camera.pendingCommand) {
            VISCAPacket message = camera.pendingCommand;
            if (lostPacketTimeout < System.currentTimeMillis() - message.getInFlight()) {
              // if VISCAPacket has not been sent, set to waiting
              byte[] payload = message.getPayload();
              if (-1 == message.getInFlight()) {
                camera.issuedMsg.put(message.getSequenceNumber(), message);
              } else {
                Logger.error("Packet got lost: " + ByteUtils.byteArrayToHex(payload, payload.length));
              }            
              try {
                DatagramPacket sendPacket = new DatagramPacket(payload, payload.length, camera.address, camera.getPort());
                UDPSocket.send(sendPacket);
                message.setInFlight(System.currentTimeMillis());
                camera.lastPacketSent = System.currentTimeMillis();
                camera.lastSentWasInquiry = false;
                Logger.trace("Packet sent: " + ByteUtils.byteArrayToHex(payload, payload.length));
                sent = true;
              } catch (IOException e) {
                Logger.error("Error while sending data: " + ByteUtils.byteArrayToHex(payload, payload.length), e);
              }
            }
          }    	
        return sent;
    }
  }

  class MessageReciever implements Runnable {

    public boolean running = true;
    private byte[] buf = new byte[256];

    @Override
    public void run() {
      while (running) {
        try {
          DatagramPacket packet = new DatagramPacket(buf, buf.length);
          UDPSocket.receive(packet);
          packetReceived(packet);
        } catch (IOException ex) {
          Logger.error(ex,
                  "Exception in UDP listener thread. Listener exiting. The service will not read network messages anymore!");
        }
      }
    }
  }

}
