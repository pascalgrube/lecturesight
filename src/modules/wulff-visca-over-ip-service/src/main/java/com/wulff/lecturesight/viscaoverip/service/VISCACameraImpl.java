package com.wulff.lecturesight.viscaoverip.service;

import com.wulff.lecturesight.visca.api.CameraPosition;
import com.wulff.lecturesight.viscaoverip.protocol.ByteUtils;
import com.wulff.lecturesight.viscaoverip.protocol.Message;
import com.wulff.lecturesight.viscaoverip.protocol.VISCA;
import com.wulff.lecturesight.viscaoverip.protocol.VISCA.MessageType;
import com.wulff.lecturesight.viscaoverip.protocol.VISCA.PayloadType;
import com.wulff.lecturesight.viscaoverip.protocol.VISCAPacket;
import com.wulff.lecturesight.viscaoverip.protocol.VISCAReplyPacket;

import org.pmw.tinylog.Logger;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

import cv.lecturesight.ptz.api.CameraListener;
import cv.lecturesight.ptz.api.PTZCamera;
import cv.lecturesight.ptz.api.PTZCameraProfile;
import cv.lecturesight.util.geometry.Position;

public class VISCACameraImpl implements PTZCamera {

  InetAddress address;
  int port;
  int numSockets;

  Timer timer;

  String model_name;

  Limits lim_pan, lim_tilt, lim_zoom;
  Limits speed_pan, speed_tilt, speed_zoom;

  int seq_num = 0; // sequence number for next package send
  ByteBuffer seq_num_buf;

  CameraState state;
  CameraPosition target;

  VISCAServiceImpl parent;

  PTZCameraProfile profile;

  VISCAPacket pendingCommand = null;
  VISCAPacket pendingInquiry = null;
  Deque<Message> queuedCommands; // queue of commands that should be issued
  Deque<Message> queuedInquiries; // queue of inquiries that should be issued
  HashMap<Integer, VISCAPacket> issuedMsg; // hash of commands/inquiries that have been send and are waiting for ACK/Complete

  VISCAPacket[] sockets; // model that mirrors the state of the camera's command buffer

  long lastUpdate; // time millis of last position update

  long lastPacketSent = -1;
  boolean lastSentWasInquiry = true;
  
  List<CameraListener> observers;

  boolean updatePollFocus = false;
  AtomicBoolean positionUpdateRequested = null;

  int last_pan = -1;
  int last_tilt = -1;

  boolean initialized = false;
  
  public VISCACameraImpl(InetAddress address, int port, Properties profile) {
    this.address = address;
    this.port = port;
    this.state = new CameraState();
    this.issuedMsg = new HashMap<Integer, VISCAPacket>();
    this.queuedCommands = new LinkedList<Message>();
    this.queuedInquiries = new LinkedList<Message>();
    this.lastUpdate = 0l;
    this.observers = new LinkedList<CameraListener>();
    this.positionUpdateRequested = new AtomicBoolean(false);
    loadProfile(profile);
    // socket count is only available after reading the profile
    this.sockets = new VISCAPacket[numSockets];
    seq_num_buf = ByteBuffer.allocate(4);
    seq_num_buf.order(ByteOrder.BIG_ENDIAN);
    resetCamera();
  }

  /**
   * Sends the CAM_VersionInq command to the camera.
   *
   */

  private void send_CamInfoInquiry() {
    Logger.info("Sending camera info inquiry command to " + address);
    Message msg = VISCA.INQ_CAM_VERSION.clone();
    queuedInquiries.add(msg);
  }

  public void updatePosition() {
	if(initialized){
		if(positionUpdateRequested.compareAndSet(false, true)){
		  Message inq_msg = VISCA.INQ_PAN_TILT_POS.clone();
		  Message inq_focus = VISCA.INQ_FOCUS_POS.clone();
		  Logger.trace("Requesting camera position update");
		  queuedInquiries.add(inq_msg);
		
		  if (updatePollFocus) {
		    Logger.trace("Requesting camera focus update");
		    inq_focus.getBytes()[0] = (byte) (VISCA.ADR_CAMERA_N + 1);
		    queuedInquiries.add(inq_focus);
		  }
		}
	}
  }

  void cancelMovement() {

    Logger.debug("Cancel movement");

    for (int i = 0; i < numSockets; i++) { 
      VISCAPacket p = sockets[i];
      if (p != null) {
        Message cancel = VISCA.NET_COMMAND_CANCEL.clone();
        byte[] pkg = cancel.getBytes();
        pkg[1] += i+1;
        queuedCommands.addFirst(cancel); // Make sure to run cancel before other commands follow
      }
    }
  }

  private void handleError(VISCAReplyPacket reply) {
    // TODO remove movement commands from pendingMsg!
    // TODO what to do with commands in issuedMsg (that did not receive ACK yet)

    String msg = "Camera at " + address + " gave error: ";
    byte[] payload = reply.getRawData();
    switch (payload[2]) {
      case 0x01:
        msg += "Message length error";
        break;
      case 0x02:
        msg += "Syntax Error";
        break;
      case 0x03:
        msg += "Command Buffer Full";
        break;
      case 0x04:
        int socket = ByteUtils.low(payload[1]);
        sockets[socket] = null;
        msg += "Command Canceled (socket: " + socket + ")";
        break;
      case 0x05:
        msg += "No Socket to be canceled (socket: " + ByteUtils.low(payload[1]) + ")";
        break;
      case 0x41:
        msg += "Command Not Executable (socket: " + ByteUtils.low(payload[1]) + ")";
        break;
      default:
        msg += "Unknown error code " + ByteUtils.byteToHex(payload[1]);
    }
    Logger.warn(msg);
    VISCAPacket packet = issuedMsg.get(reply.getSequencenumber());
	switch(packet.getMessage().getPayloadType()){
	case VISCA_COMMAND:
		pendingCommand = null;
		break;
	case VISCA_INQUIRY:
		pendingInquiry = null;
	    positionUpdateRequested.set(false);
		break;	
	default:
		Logger.warn("Strange Payload type");
	}
	issuedMsg.remove(packet.getSequenceNumber());
	Logger.trace("Finished handling error");
  }

  private void handleControlReply(VISCAReplyPacket reply) {
    if(1 == reply.getRawData().length){
      //success -> only command available is reset sequence number
      seq_num = 1;
      issuedMsg.remove(reply.getSequencenumber());
      pendingCommand = null;
      initialized = true;
    } else {
        Logger.error("BROKEN CONTROL REPLY RECEIVED " + address + "length:" + reply.getRawData().length
                + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
    }
  }

  private void handleControlCommand(VISCAReplyPacket reply) {
	    if(1 == reply.getRawData().length){
	        Logger.error("RECEIVED RESET COMMAND from" + address + "length:" + reply.getRawData().length
	                + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
	    } else {
	      if( 0x0F == reply.getRawData()[0]){
	    	  switch (reply.getRawData()[1]) {
			case 0x01:
				//Abnormal sequence number
			    VISCAPacket packet = issuedMsg.get(reply.getSequencenumber());
				switch(packet.getMessage().getPayloadType()){
				case VISCA_COMMAND:
					queuedCommands.addFirst(packet.getMessage());
					pendingCommand = null;
					break;
				case VISCA_INQUIRY:
					queuedInquiries.addFirst(packet.getMessage());
					pendingInquiry = null;
					break;	
				default:
					Logger.warn("Strange Payload type");
				}
				issuedMsg.remove(packet.getSequenceNumber());
		        Logger.warn("Wrong sequence number. Resending message: " + address + "length:" + reply.getRawData().length
		                + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
				break;
			case 0x02:
				//Abnormal message
		        Logger.error("ABNORMAL MESSAGE " + address + "length:" + reply.getRawData().length
		                + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
				break;
			default:
				break;
			}
	      } else {
	          Logger.error("BROKEN CONTROL COMMAND RECEIVED " + address + "length:" + reply.getRawData().length
	                  + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
	      }
	    }
	  }

  
  private void handleACKCompletion(VISCAReplyPacket reply) {
    String msg = "";
    int socket = ByteUtils.low(reply.getRawData()[1]);
    if (ByteUtils.high(reply.getRawData()[1]) == 4) {
      // set socket of camera used
      VISCAPacket p = issuedMsg.get(reply.getSequencenumber());
      sockets[socket - 1] = p;
      p.setAcknowledged(true);
      msg = "ACK";
      pendingCommand = null;
      Logger.trace("Current pending command: " + pendingCommand);  
    } else if (ByteUtils.high(reply.getRawData()[1]) == 5) {
      // set socket of camera unused
      sockets[socket - 1] = null;
      issuedMsg.remove(reply.getSequencenumber());

      msg = "Completion";
    }
    msg += " from #" + address + " (socket: " + socket + ")";

    Logger.trace(msg);
    
  }

  private void handlePositionUpdate(VISCAReplyPacket reply) {

    Logger.trace("Received camera position update");

    byte[] buffer = reply.getRawData();

    int pan = ((buffer[2] & 0x0f) << 12) + ((buffer[3] & 0x0f) << 8) + ((buffer[4] & 0x0f) << 4) + (buffer[5] & 0x0f);
    int tlt = ((buffer[6] & 0x0f) << 12) + ((buffer[7] & 0x0f) << 8) + ((buffer[8] & 0x0f) << 4) + (buffer[9] & 0x0f);

    // this assumes that the camera uses the lower half of the 16bit value range
    // for right side and upper half for left side
    pan = pan > 0x7fff ? pan - 0xffff : pan;

    // same as above for tilt
    tlt = tlt > 0x7fff ? tlt - 0xffff : tlt;

    try {
      CameraPosition pos = state.currentPosition();
      Logger.debug("handling position update");
      if (last_pan != pan || last_tilt != tlt) {
        Logger.debug("Camera position updated: last_pan=" + last_pan + " last_tilt=" + last_tilt + " new pan=" + pan
                + " new tilt=" + tlt);
        last_pan = pan;
        last_tilt = tlt;
      }
      pos.set(pan, tlt);
      notifyCameraListeners();
    } catch (Exception e) {
      e.printStackTrace();
    }
    positionUpdateRequested.set(false);
  }

  private void handleCameraFocusReply(VISCAReplyPacket reply) {

    byte[] buffer = reply.getRawData();
    int focus = ((buffer[2] & 0x0f) << 12) + ((buffer[3] & 0x0f) << 8) + ((buffer[4] & 0x0f) << 4) + (buffer[5] & 0x0f);

    state.focus = focus;

    Logger.trace("Received camera focus position update: camera " + address + " focus " + focus);
  }

  private void handleCameraZoomReply(VISCAReplyPacket reply) {

  }

  private void handleCameraInfoReply(VISCAReplyPacket reply) {
    Logger.info("Received camera info from #" + address);
    // createCamera(adr, ByteUtils.trimArray(buffer, buffer.length));
  }

  private void handleInquiryReply(VISCAReplyPacket reply) {
    VISCAPacket request = issuedMsg.get(reply.getSequencenumber());
    issuedMsg.remove(reply.getSequencenumber());
    pendingInquiry = null;
    switch (request.getMessage().getMessageType()) {
      case INQ_CAM_VERSION:
        handleCameraInfoReply(reply);
        break;
      case INQ_FOCUS_POS:
        handleCameraFocusReply(reply);
        break;
      case INQ_PAN_TILT_POS:
        handlePositionUpdate(reply);
        break;
      case INQ_ZOOM_POS:
        handleCameraZoomReply(reply);
        break;
      default:
        Logger.error("Received reply for unsupported Inquiry method" + address + "length:" + reply.getRawData().length
                + " :: " + ByteUtils.byteArrayToHex(reply.getRawData(), reply.getRawData().length) + ":: ");
    }
  }

  void handleResponse(byte[] response) {

    boolean packetValid = true;

    PayloadType messageType = null;
    switch (response[0]) {
      case 0x01:
        switch (response[1]) {
          case 0x00:
            messageType = PayloadType.VISCA_COMMAND;
            break;
          case 0x10:
            messageType = PayloadType.VISCA_INQUIRY;
            break;
          case 0x11:
            messageType = PayloadType.VISCA_REPLY;
            break;
          case 0x20:
            messageType = PayloadType.VISCA_SET_DEVICE;
            break;
          default:
            messageType = PayloadType.ILLEGAL_MESSAGE;
            break;
        }
        ;
        break;
      case 0x02:
        switch (response[1]) {
          case 0x00:
            messageType = PayloadType.CONTROL_COMMAND;
            break;
          case 0x01:
            messageType = PayloadType.CONTROL_REPLY;
            break;
          default:
            messageType = PayloadType.ILLEGAL_MESSAGE;
        }
        ;
        break;
      default:
        messageType = PayloadType.ILLEGAL_MESSAGE;
    }

    if (PayloadType.ILLEGAL_MESSAGE == messageType) {
      Logger.error("Illegal message type type in Packet: " + address + "length:" + response.length + " :: "
              + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
      packetValid = false;
    }

    if (0 != response[2]) {
      Logger.error("Illegal date at pos 2 type in Packet" + address + "length:" + response.length + " :: "
              + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
      packetValid = false;
    }
    int messageSize = response[3];
    if (messageSize < 1 || 16 < messageSize || response.length - 8 != messageSize) {
      Logger.error("Illegal  messagesize: " + messageSize + " at Pos 3 type in Packet" + address + "length:"
              + response.length + " :: " + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
      packetValid = false;
    }

    ByteBuffer sequenceNummerBuffer = ByteBuffer.allocate(4);
    sequenceNummerBuffer.put(response, 4, 4);
    sequenceNummerBuffer.flip();
    int sequenceNumber = sequenceNummerBuffer.getInt();

    if (!issuedMsg.containsKey(sequenceNumber)) {
      Logger.error("Illegal  sequencenumber: No request with " + sequenceNumber + " sent.  Packet" + address + "length:"
              + response.length + " :: " + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
      Logger.error(issuedMsg);
      packetValid = false;
    }

    if (packetValid) {
      VISCAReplyPacket reply = new VISCAReplyPacket();
      reply.setType(messageType);
      reply.setSequencenumber(sequenceNumber);
      byte[] payload = new byte[messageSize];
      copyPart(response, payload, 8, messageSize);
      reply.setRawData(payload);

      switch (messageType) {
      	case CONTROL_REPLY:
          handleControlReply(reply);
          break;
      	case CONTROL_COMMAND:
            handleControlCommand(reply);
            break;
      	case VISCA_REPLY:
          // check if target is correct (90)
          if ((byte) 0x90 == payload[0] && (byte) 0xFF == payload[payload.length - 1]) {
            if (ByteUtils.high(payload[1]) == 6 && 4 == payload.length) {
              handleError(reply);
            } else {
              if (3 == payload.length && (ByteUtils.high(payload[1]) == 4 || ByteUtils.high(payload[1]) == 5)) {
                handleACKCompletion(reply);
              } else {
                if (ByteUtils.high(payload[1]) == 5) {
                  handleInquiryReply(reply);
                } else {
                  packetValid = false;
                  Logger.error("Illegal payload: in VISCA REPLY from: " + address + "length:" + response.length + " :: "
                          + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
                }
              }
            }
          }
          break;
        default:
          Logger.error("Packet was not a REPLY from: " + address + "length:" + response.length + " :: "
                  + ByteUtils.byteArrayToHex(response, response.length) + ":: ");
          break;
      }
    }
  }

  // TODO replace with System.arrayCopy (?)
  void copyPart(byte[] src, byte[] dest, int offset, int length) {
    int glength = offset + length;
    int dp = 0;
    for (int sp = offset; sp < glength; sp++) {
      dest[dp++] = src[sp];
    }
  }

  void clearInterface(InetAddress adr) {
    Logger.debug("Clear interface");
    Message msg = VISCA.NET_IF_CLEAR.clone();
    queuedCommands.add(msg);
  }

  private void resetCamera() {
    Message reset = VISCA.CTRL_RESET.clone();
    queuedCommands.add(reset);
  }

  final void loadProfile(Properties profile) {
    this.profile = new PTZCameraProfile(profile);
    model_name = stringOrDie(Constants.PROFKEY_MODEL_NAME, profile);
    lim_pan = new Limits(intOrDie(Constants.PROFKEY_PAN_MIN, profile), intOrDie(Constants.PROFKEY_PAN_MAX, profile));
    lim_tilt = new Limits(intOrDie(Constants.PROFKEY_TILT_MIN, profile), intOrDie(Constants.PROFKEY_TILT_MAX, profile));
    lim_zoom = new Limits(intOrDie(Constants.PROFKEY_ZOOM_MIN, profile), intOrDie(Constants.PROFKEY_ZOOM_MAX, profile));
    speed_pan = new Limits(0, intOrDie(Constants.PROFKEY_PAN_MAXSPEED, profile));
    speed_tilt = new Limits(0, intOrDie(Constants.PROFKEY_TILT_MAXSPEED, profile));
    speed_zoom = new Limits(0, intOrDie(Constants.PROFKEY_ZOOM_MAXSPEED, profile));
    numSockets = intOrDie(Constants.PROFKEY_SOCKETS_COUNT, profile);
  }

  int intOrDie(String key, Properties props) {
    if (!props.containsKey(key)) {
      String msg = "Failed loading value " + key + " from camera profile. Key not existing.";
      Logger.warn(msg);
      throw new IllegalArgumentException(msg);
    }
    return Integer.parseInt(props.getProperty(key));
  }

  String stringOrDie(String key, Properties props) {
    if (!props.containsKey(key)) {
      String msg = "Failed loading value " + key + " from camera profile. Key not existing.";
      Logger.warn(msg);
      throw new IllegalArgumentException(msg);
    }
    return props.getProperty(key);
  }

  public boolean interfaceReady() {
    for (int i = 1; i < sockets.length; i++) { // start with index 1 since 0 is a pseudo-socket (for inquiries)
      if (sockets[i] == null) {
        return true;
      }
    }
    return false;
  }

  CameraPosition clampPosition(CameraPosition p) {
    return new CameraPosition(lim_pan.clamp(p.x()), lim_tilt.clamp(p.y()));
  }

  /**
   * Increments the sequence number and returns a big endian representation of of the current sequence number.
   * 
   * @return big endian representation of current sequence number
   */
  private byte[] getSequenceNumberAsByteArray() {
    seq_num_buf.putInt(seq_num);
    seq_num_buf.flip();
    return seq_num_buf.array();
  }

  @Override
  public void zoom(int zoom) {

    Logger.debug("Zoom to " + zoom);

    Message msg = VISCA.CMD_ZOOM.clone();
    byte[] pkg = msg.getBytes();

    // set zoom target
    byte[] a = ByteUtils.s2b((short) zoom);
    pkg[4] = a[0];
    pkg[5] = a[1];
    pkg[6] = a[2];
    pkg[7] = a[3];

    queuedCommands.add(msg);
  }

  public int getPort() {
    return port;
  }

  /**
   * Sends the content of <code>b</code> over the serial port. Method is synchronized so that competing calls are
   * enqueued.
   *
   * @param a
   */
  synchronized void fillPending() {
	  if (pendingInquiry == null && !queuedInquiries.isEmpty()) {
		  Message m = queuedInquiries.poll();
		  pendingInquiry = createPacket(m);
	  }
	  
    if (pendingCommand == null && !queuedCommands.isEmpty()) {
      Message m = queuedCommands.peek();
      boolean canRun = false;
      switch (m.getPayloadType()) {
        case VISCA_COMMAND:
          if (MessageType.NET_COMMAND_CANCEL == m.getMessageType()) {
            canRun = true;
          } else {
            if (sockets[0] == null || sockets[1] == null) {
              canRun = true;
            }
          }
          break;
        case CONTROL_COMMAND:
          canRun = true;
          break;
        case VISCA_SET_DEVICE:
          canRun = true;
          break;
        default:
          Logger.error("Got a reply as Payload-Type");
      }
      if (canRun) {
        m = queuedCommands.poll();
        pendingCommand = createPacket(m);
      }
    }
  }

  private synchronized VISCAPacket createPacket(Message m){
      int packet_len = m.getBytes().length + 8;
      byte[] buffer = new byte[packet_len];
      // build header
      System.arraycopy(m.getPayloadType().getCode(), 0, buffer, 0, 2); // 0-1: message type
      // payload length
      buffer[2] = 0x00;
      buffer[3] = (byte) m.getBytes().length;
      // sequence number
      System.arraycopy(getSequenceNumberAsByteArray(), 0, buffer, 4, 4);
      // payload
      System.arraycopy(m.getBytes(), 0, buffer, 8, m.getBytes().length);
      Logger.trace(" >>" + ByteUtils.byteArrayToHex(buffer, packet_len));
      // Create the package to be send
      VISCAPacket packet = new VISCAPacket();
      packet.setSequenceNumber(seq_num);
      packet.setMessage(m);
      packet.setPayload(buffer);      
      seq_num++;
	  return packet;
  }
  
  @Override
  public void focus(int focus) {

    Logger.debug("Focus to " + focus);

    Message msg = VISCA.CMD_FOCUS_DIRECT.clone();
    byte[] pkg = msg.getBytes();

    // set zoom target
    byte[] a = ByteUtils.s2b((short) focus);
    pkg[4] = a[0];
    pkg[5] = a[1];
    pkg[6] = a[2];
    pkg[7] = a[3];

    queuedCommands.add(msg);

  }

  @Override
  public void focusMode(FocusMode mode) {

    // STOP, NEAR, FAR, MANUAL, AUTO
    Logger.debug("Focus mode to " + mode);

    Message msg = null;

    switch (mode) {
      case STOP:
        msg = VISCA.CMD_FOCUS_STOP.clone();
        break;
      case NEAR:
        msg = VISCA.CMD_FOCUS_NEAR.clone();
        break;
      case FAR:
        msg = VISCA.CMD_FOCUS_FAR.clone();
        break;
      case MANUAL:
        msg = VISCA.CMD_FOCUS_MANUAL.clone();
        break;
      case AUTO:
        msg = VISCA.CMD_FOCUS_AUTO.clone();
        break;
    }

    queuedCommands.add(msg);

  }

  @Override
  public String getName() {
    return model_name;
  }

  @Override
  public PTZCameraProfile getProfile() {
    return profile;
  }

  @Override
  public void reset() {
  }

  @Override
  public void stopMove() {
	  if (!initialized) return;
    Logger.debug("Stop movement");

    Message msg = VISCA.CMD_STOP_MOVE.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) VISCA.DEFAULT_SPEED;
    pkg[5] = (byte) VISCA.DEFAULT_SPEED;
    queuedCommands.add(msg);
  }

  @Override
  public void moveHome() {
	  if (!initialized) return;
    Logger.debug("Move home");
    Message msg = VISCA.CMD_MOVE_HOME.clone();
    queuedCommands.add(msg);
  }

  @Override
  public void movePreset(int preset) {
	  if (!initialized) return;
    Logger.debug("Move preset " + preset);

    Message msg = VISCA.CMD_MOVE_PRESET.clone();
    byte[] pkg = msg.getBytes();

    // set preset
    pkg[5] = (byte) preset;

    queuedCommands.add(msg);
  }

  @Override
  public void moveUp(int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move up speed " + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_UP.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) VISCA.DEFAULT_SPEED;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveDown(int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move down speed " + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_DOWN.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) VISCA.DEFAULT_SPEED;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveLeft(int panSpeed) {
	if (!initialized) return;
	  
    Logger.debug("Move left speed " + panSpeed);

    Message msg = VISCA.CMD_MOVE_LEFT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) VISCA.DEFAULT_SPEED;
    queuedCommands.add(msg);
  }

  @Override
  public void moveRight(int panSpeed) {
	  if (!initialized) return;
	  
    Logger.debug("Move right speed " + panSpeed);

    Message msg = VISCA.CMD_MOVE_RIGHT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) VISCA.DEFAULT_SPEED;
    queuedCommands.add(msg);
  }

  @Override
  public void moveUpLeft(int panSpeed, int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move up-left speed " + panSpeed + "/" + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_UP_LEFT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveUpRight(int panSpeed, int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move up-right speed " + panSpeed + "/" + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_UP_RIGHT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveDownLeft(int panSpeed, int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move down-left speed " + panSpeed + "/" + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_DOWN_LEFT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveDownRight(int panSpeed, int tiltSpeed) {
	  if (!initialized) return;
    Logger.debug("Move down-right " + panSpeed + "/" + tiltSpeed);

    Message msg = VISCA.CMD_MOVE_DOWN_RIGHT.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;
    queuedCommands.add(msg);
  }

  @Override
  public void moveAbsolute(int panSpeed, int tiltSpeed, Position target) {
	  if (!initialized) return;
    Logger.debug("Move absolute target " + target + " at pan speed " + panSpeed + ", tilt speed " + tiltSpeed);

    // cancel current movement
    // parent.cancelMovement(address);
    // parent.clearInterface(address);

    // set new movement target and speeds
    Message msg = VISCA.CMD_MOVE_ABSOLUTE.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;

    // set pan target
    int val = target.getX();
    int pan = val > 0x7fff ? val - 0xffff : val;
    byte[] a = ByteUtils.s2b((short) pan);
    pkg[6] = a[0];
    pkg[7] = a[1];
    pkg[8] = a[2];
    pkg[9] = a[3];

    // set pan target
    val = target.getY();
    int tilt = val > 0x7fff ? val - 0xffff : val;
    a = ByteUtils.s2b((short) tilt);
    pkg[10] = a[0];
    pkg[11] = a[1];
    pkg[12] = a[2];
    pkg[13] = a[3];

    queuedCommands.add(msg);
  }

  @Override
  public void moveRelative(int panSpeed, int tiltSpeed, Position target) {
	  if (!initialized) return;
    Logger.debug("Move relative target " + panSpeed + "/" + tiltSpeed);

    // set new movement target and speeds
    Message msg = VISCA.CMD_MOVE_RELATIVE.clone();
    byte[] pkg = msg.getBytes();

    // set pan/tilt speed
    pkg[4] = (byte) panSpeed;
    pkg[5] = (byte) tiltSpeed;

    // set pan target
    int val = target.getX();
    int pan = val > 0x7fff ? val - 0xffff : val;
    byte[] a = ByteUtils.s2b((short) pan);
    pkg[6] = a[0];
    pkg[7] = a[1];
    pkg[8] = a[2];
    pkg[9] = a[3];

    // set pan target
    val = target.getY();
    int tilt = val > 0x7fff ? val - 0xffff : val;
    a = ByteUtils.s2b((short) tilt);
    pkg[10] = a[0];
    pkg[11] = a[1];
    pkg[12] = a[2];
    pkg[13] = a[3];

    queuedCommands.add(msg);
  }

  @Override
  public void clearLimits() {
	  if (!initialized) return;
    Logger.debug("Clear camera limits");

    // clear down-left limit
    Message msg_dl = VISCA.CMD_LIMIT_CLEAR.clone();
    byte[] pkg = msg_dl.getBytes();
    queuedCommands.add(msg_dl);

    // clear up-right limit
    Message msg_ur = VISCA.CMD_LIMIT_CLEAR.clone();
    pkg = msg_ur.getBytes();
    pkg[5] = 1;
    queuedCommands.add(msg_ur);
  }

  @Override
  public void setLimitUpRight(int pan, int tilt) {
	  if (!initialized) return;
    Logger.debug("Set limit up-right");

    Message msg = VISCA.CMD_LIMIT_SET.clone();
    byte[] pkg = msg.getBytes();

    // set direction
    pkg[5] = 1;

    // set pan target
    pan = pan > 0x7fff ? pan - 0xffff : pan;
    byte[] a = ByteUtils.s2b((short) pan);
    pkg[6] = a[0];
    pkg[7] = a[1];
    pkg[8] = a[2];
    pkg[9] = a[3];

    // set pan target
    tilt = tilt > 0x7fff ? tilt - 0xffff : tilt;
    a = ByteUtils.s2b((short) tilt);
    pkg[10] = a[0];
    pkg[11] = a[1];
    pkg[12] = a[2];
    pkg[13] = a[3];

    queuedCommands.add(msg);
  }

  @Override
  public void setLimitDownLeft(int pan, int tilt) {
	  if (!initialized) return;
    Logger.debug("Set limit down-left");

    Message msg = VISCA.CMD_LIMIT_SET.clone();
    byte[] pkg = msg.getBytes();

    // set direction
    pkg[5] = 0;

    // set pan target
    pan = pan > 0x7fff ? pan - 0xffff : pan;
    byte[] a = ByteUtils.s2b((short) pan);
    pkg[6] = a[0];
    pkg[7] = a[1];
    pkg[8] = a[2];
    pkg[9] = a[3];

    // set pan target
    tilt = tilt > 0x7fff ? tilt - 0xffff : tilt;
    a = ByteUtils.s2b((short) tilt);
    pkg[10] = a[0];
    pkg[11] = a[1];
    pkg[12] = a[2];
    pkg[13] = a[3];

    queuedCommands.add(msg);
  }

  @Override
  public Position getPosition() {
    return new Position(this.state.currentPosition().x(), this.state.currentPosition().y());
  }

  @Override
  public void stopZoom() {
  }

  @Override
  public void zoomIn(int speed) {
  }

  @Override
  public void zoomOut(int speed) {
  }

  @Override
  public int getZoom() {
    return 0; // TODO implement!
  }

  @Override
  public int getFocus() {
    return this.state.currentFocus();
  }

  @Override
  public void cancel() {
    cancelMovement();
  }

  @Override
  public void addCameraListener(CameraListener l) {
    observers.add(l);
  }

  @Override
  public void removeCameraListener(CameraListener l) {
    observers.remove(l);
  }

  void notifyCameraListeners() {
    Position pos = new Position(state.position.x(), state.position.y());
    for (CameraListener cl : observers) {
      cl.positionUpdated(pos);
    }
  }
}
