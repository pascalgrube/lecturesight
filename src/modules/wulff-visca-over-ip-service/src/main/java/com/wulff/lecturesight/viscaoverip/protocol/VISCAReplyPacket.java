package com.wulff.lecturesight.viscaoverip.protocol;

import com.wulff.lecturesight.viscaoverip.protocol.VISCA.PayloadType;

public class VISCAReplyPacket {

  PayloadType type;
  int sequencenumber;
  byte[] rawData;
  
  public PayloadType getType() {
    return type;
  }
  public void setType(PayloadType type) {
    this.type = type;
  }
  public int getSequencenumber() {
    return sequencenumber;
  }
  public void setSequencenumber(int sequencenumber) {
    this.sequencenumber = sequencenumber;
  }
  public byte[] getRawData() {
    return rawData;
  }
  public void setRawData(byte[] rawData) {
    this.rawData = rawData;
  }
  
}
