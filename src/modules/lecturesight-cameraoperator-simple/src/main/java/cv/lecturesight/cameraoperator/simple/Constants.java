package cv.lecturesight.cameraoperator.simple;

public class Constants {
  static final String PROPKEY_TARGET_TIMEOUT = "target.timeout";
  static final String PROPKEY_TRACKING_TIMEOUT = "tracking.timeout";
  static final String PROPKEY_PAN = "pan";
  static final String PROPKEY_TILT = "tilt";
  static final String PROPKEY_ZOOM = "zoom";
  static final String PROPKEY_FRAME_WIDTH = "frame.width";
  static final String PROPKEY_IDLE_PRESET = "idle.preset";
  static final String PROPKEY_TARGET_LIMIT = "target.limit";
}
