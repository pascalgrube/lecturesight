# Pan-only Camera Operator

### cv.lecturesight.cameraoperator.panonly.pan

*Default:* 0.0 (-1.0 to 1.0)

Sets the initial pan position.

### cv.lecturesight.cameraoperator.panonly.tilt

*Default:* 0.0

Sets the initial tilt position.

### cv.lecturesight.cameraoperator.panonly.zoom

*Default:* 0.0

Sets the initial zoom position.

### cv.lecturesight.cameraoperator.panonly.target.timeout

*Default:* 2500

Sets the time in milliseconds after the last target movement after which a target will no longer be tracked.

### cv.lecturesight.cameraoperator.panonly.tracking.timeout

*Default:* 60000 (0 to disable)

Sets the time in milliseconds after the last target movement to return to the initial tracking position.

