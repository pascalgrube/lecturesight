package cv.lecturesight.cameraoperator.scripted;

public class Constants {
  
  final static String PROPKEY_SCRIPTFILE = "script";
  final static String PROPKEY_SCRIPTDIR = "scriptdir";
  final static String PROPKEY_TIMEOUT = "worker.timeout";
  
}
